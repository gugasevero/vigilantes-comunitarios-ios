//
//  UsuarioViewController.swift
//  Vigilantes Comunitarios
//
//  Created by Gustavo Severo on 31/10/17.
//  Copyright © 2017 Gustavo Severo. All rights reserved.
//

import UIKit

class UsuarioViewController: UIViewController {
    
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var sobrenomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    
    var idcep: Int!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func salvarUsuario(_ sender: Any) {
        let idcep: Int = self.idcep!
        let nome: String!
        let sobrenome: String!
        let email: String!
        let senha: String!
        
        nome = self.nomeTextField.text
        sobrenome = self.sobrenomeTextField.text
        email = self.emailTextField.text
        senha = self.senhaTextField.text
        
        let parameters = [
            "idcep": idcep,
            "nome": nome,
            "sobrenome": sobrenome,
            "email": email,
            "senha": senha,
            "opcao": NSNumber(value: 1)] as Dictionary<String, AnyObject>
        
        print("Idcep: ", idcep, "Nome: ", nome, " Sobrenome: ", sobrenome, " Email: ", email, " Senha: ", senha)
        
        //create the url with URL
        let url = URL(string: "http://www.vigilantescomunitarios.com/php/salvaUsuario.php")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            //print("Dados", request) dados salvos vindo no request
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            print("Data: ", data)
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }



}
