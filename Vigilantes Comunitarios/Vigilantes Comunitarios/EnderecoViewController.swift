//
//  EnderecoViewController.swift
//  Vigilantes Comunitarios
//
//  Created by Gustavo Severo on 30/10/17.
//  Copyright © 2017 Gustavo Severo. All rights reserved.
//

import UIKit

class EnderecoViewController: UIViewController {

    @IBOutlet weak var cepTextField: UITextField!
    @IBOutlet weak var logradouroTextField: UITextField!
    @IBOutlet weak var bairroTextField: UITextField!
    @IBOutlet weak var cidadeTextField: UITextField!
    @IBOutlet weak var estadoTextField: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func getCep(_ sender: Any) {
        var cep = ""
        cep = cepTextField.text!
        //print("O CEP é:", cep)
        
        let urlPath = URL(string: "https://viacep.com.br/ws/"+cep+"/json")
        let session = URLSession.shared
        var request = URLRequest(url: urlPath!)
        
        request.timeoutInterval = 60
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if((error) != nil) {
                print(error!.localizedDescription)
            } else {
                do {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    
                    DispatchQueue.main.async(execute: {
                        let dic: NSDictionary = (jsonResult as NSDictionary);
                        //print("Dados", jsonResult)
                        if dic.count > 1 {
                            self.cepTextField.text = jsonResult["cep"] as? String
                            self.bairroTextField.text = jsonResult["bairro"] as? String
                            self.cidadeTextField.text = jsonResult["localidade"] as? String
                            self.logradouroTextField.text = jsonResult["logradouro"] as? String
                            self.estadoTextField.text = jsonResult["uf"] as? String
                            
                        } else {
                            //self.preencheDados(nil)
                            print("CEP inexistente")
                        }
                    })
                } catch {
                    DispatchQueue.main.async(execute: {
                        //self.preencheDados(nil)
                    })
                }
            }
            
        }).resume()
        
    }
    
    @IBAction func salvarEndereco(_ sender: Any) {
        
//   print("cep", cepTextField.text!, "bairro", bairroTextField.text!, "cidade", cidadeTextField.text!, "logradouro", logradouroTextField.text!, "estado", estadoTextField.text!)
        
        let parameters = [
            "cep": cepTextField.text!,
            "logradouro": logradouroTextField.text!,
            "bairro": bairroTextField.text!,
            "cidade": cidadeTextField.text!,
            "uf": estadoTextField.text!,
            "opcao": NSNumber(value: 1)] as Dictionary<String, AnyObject>
        
        //create the url with URL
        let url = URL(string: "http://www.vigilantescomunitarios.com/php/salvaEndereco.php")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            //print("Dados", request) dados salvos vindo no request
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            print("Data: ", data)
         
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {

                    guard let ids: AnyObject = json["idCep"] as AnyObject  else {
                        
                        return
                    }
                    guard  let idcep = ids as? String else{
                        return
                    }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier :"UsuarioViewController") as! UsuarioViewController

                    viewController.idcep = Int(idcep)
                    DispatchQueue.main.async(){
                        self.present(viewController, animated: true)
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    


}
